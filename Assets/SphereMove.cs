using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereMove : MonoBehaviour
{
    [SerializeField] internal float pingPongT = 0.5f;
    internal float pingPongMax = 1;
    internal Vector3 startPos;
    internal Vector3 endPos;
    internal float sizeOffset = 0.2f;

    private void Start()
    {
        startPos = new Vector3(LevelManager.rightLanePosX + sizeOffset, transform.position.y, transform.position.z);
        endPos = new Vector3(LevelManager.leftLanePosX - sizeOffset, transform.position.y, transform.position.z);
    }
    void Update()
    {
        startPos.z = transform.position.z;
        endPos.z = transform.position.z;
        float lerpTime = Mathf.PingPong(Time.time * pingPongT, pingPongMax);
        transform.position = Vector3.Lerp(startPos, endPos, lerpTime);
    }
}
