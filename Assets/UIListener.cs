using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIListener : MonoBehaviour
{
    internal int currentCoins = 0;
    internal int coinValue = 15;
    internal int currentScore = 0;
    internal int curDist = 0;
    internal int prevDist = 0;
    internal int scoreDist = 0;
    internal int distValue = 5;
    internal int curLives = 3;

    LevelManager levelManager;

    internal void OnEnable()
    {
        EventManager.StartListening("getCoin", CoinUp);
        EventManager.StartListening("getHit", LivesUp);
        EventManager.StartListening("GameOver", GameOver);
    }

    internal void OnDisable()
    {
        EventManager.StopListening("getCoin", CoinUp);
        EventManager.StopListening("getHit", LivesUp);
        EventManager.StopListening("GameOver", GameOver);
    }

    internal void CoinUp(Dictionary<string, object> message)
    {
        currentCoins++;
        GameObject.FindGameObjectWithTag("coin").GetComponent<textChanger>().UpdateText(currentCoins.ToString());
        ScoreUp(coinValue);
    }

    internal void LivesUp(Dictionary<string, object> message)
    {
        curLives--;
        GameObject.FindGameObjectWithTag("lives").GetComponent<textChanger>().UpdateText(curLives.ToString());
    }
    internal void GameOver(Dictionary<string, object> message)
    {
        Debug.Log(message["state"].ToString());
        if (message["state"].ToString() == "lose")
        {
            Debug.Log("Lost!");
            GameObject.FindGameObjectWithTag("gameover").GetComponent<textChanger>().UpdateText("Game Over");
        }
        else if (message["state"].ToString() == "win")
        {
            Debug.Log("Win!");
            GameObject.FindGameObjectWithTag("gameover").GetComponent<textChanger>().UpdateText("You Win!");
        }
    }

    //Potentially change this so you can pass an enum for the tag so you can turn the UpdateText into its own function
    void ScoreUp(int scoreValue)
    {
        currentScore += scoreValue;
        GameObject.FindGameObjectWithTag("score").GetComponent<textChanger>().UpdateText(currentScore.ToString());
    }

    void DistUp(int distance)
    {
        GameObject.FindGameObjectWithTag("distance").GetComponent<textChanger>().UpdateText(distance.ToString());
    }



    private void Start()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
    }
    private void Update()
    {
        curDist = levelManager.levelProgress;
        scoreDist = curDist - prevDist;
        prevDist = curDist;
        DistUp(curDist);
        ScoreUp(scoreDist * distValue);
    }
}
