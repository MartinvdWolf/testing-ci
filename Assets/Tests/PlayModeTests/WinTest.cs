using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using NUnit.Framework.Interfaces;



public class StateTest
{
    public string goMessage;

    [UnitySetUp]
    public IEnumerator UnitySetUp()
    {
        yield return EditorSceneManager.LoadSceneAsyncInPlayMode("./Assets/Scenes/Stage.unity", new LoadSceneParameters(LoadSceneMode.Single));
    }

    [UnityTest]
    public IEnumerator WinTest()
    {
        // Use the Assert class to test conditions.
        // Use yield to skip a frame.
        EventManager.StartListening("GameOver", GameOver);
        yield return null;
        EventManager.TriggerEvent("GameOver", new Dictionary<string, object> { { "state", "win" } });
        yield return null;
        Assert.AreEqual(goMessage, "win");
    }

    [UnityTest]
    public IEnumerator LoseTest()
    {
        // Use the Assert class to test conditions.
        // Use yield to skip a frame.
        EventManager.StartListening("GameOver", GameOver);
        yield return null;
        EventManager.TriggerEvent("GameOver", new Dictionary<string, object> { { "state", "lose" } });
        yield return null;
        Assert.AreEqual(goMessage, "lose");
    }
    void GameOver(Dictionary<string, object> message)
    {
        goMessage = message["state"].ToString();
    }

}
