using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using NUnit.Framework.Interfaces;

public class MovementTest
{
    static int sceneLoadDelay = 1;
    static int movementWait = 2;
    PlayerListener.Lanes curLane;

    [UnitySetUp]
    public IEnumerator UnitySetUp()
    {
        yield return EditorSceneManager.LoadSceneAsyncInPlayMode("./Assets/Scenes/Stage.unity", new LoadSceneParameters(LoadSceneMode.Single));
    }

    [UnityTest]
    //[LoadScene("./ Assets / Scenes / Stage.unity")]
    public IEnumerator MoveRightWithEnumeratorPasses()
    {
        //Scene loading grace period
        yield return new WaitForSeconds(sceneLoadDelay);

        //PlayerListener on Player uses enums for lanes, direction being a number is required to observe the correct outcome
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", 1 } });
        
        yield return new WaitForSeconds(movementWait);
        curLane = GameObject.Find("Player").GetComponent<PlayerListener>().myLane;

        Assert.AreEqual(PlayerListener.Lanes.Right, curLane);
    }

    [UnityTest]

    public IEnumerator MoveLeftWithEnumeratorPasses()
    {
        //Scene loading grace period
        yield return new WaitForSeconds(sceneLoadDelay);

        //PlayerListener uses enums for lanes, direction being a number is required to observe the correct outcome
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", -1 } });

        yield return new WaitForSeconds(movementWait);
        curLane = GameObject.Find("Player").GetComponent<PlayerListener>().myLane;

        Assert.AreEqual(PlayerListener.Lanes.Left, curLane);
    }

    [UnityTest]

    public IEnumerator RightMoveToMidWithEnumeratorPasses()
    {

        //Scene loading grace period
        yield return new WaitForSeconds(sceneLoadDelay);

        //PlayerListener uses enums for lanes, direction being a number is required to observe the correct outcome
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", 1 } });

        yield return new WaitForSeconds(movementWait);
        curLane = GameObject.Find("Player").GetComponent<PlayerListener>().myLane;

        Assert.AreEqual(PlayerListener.Lanes.Right, curLane);

        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", -1 } });

        yield return new WaitForSeconds(movementWait);
        curLane = GameObject.Find("Player").GetComponent<PlayerListener>().myLane;

        Assert.AreEqual(PlayerListener.Lanes.Mid, curLane);
    }

    [UnityTest]

    public IEnumerator LeftMoveToMidWithEnumeratorPasses()
    {
        //Scene loading grace period
        yield return new WaitForSeconds(sceneLoadDelay);

        //PlayerListener uses enums for lanes, direction being a number is required to observe the correct outcome
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", -1 } });
        yield return new WaitForSeconds(movementWait);
        curLane = GameObject.Find("Player").GetComponent<PlayerListener>().myLane;

        Assert.AreEqual(PlayerListener.Lanes.Left, curLane);
        
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", 1 } });
        yield return new WaitForSeconds(movementWait);
        curLane = GameObject.Find("Player").GetComponent<PlayerListener>().myLane;

        Assert.AreEqual(PlayerListener.Lanes.Mid, curLane);
    }
    public class LoadSceneAttribute : NUnitAttribute, IOuterUnityTestAction
    {
        private string scene;

        public LoadSceneAttribute(string scene) => this.scene = scene;

        IEnumerator IOuterUnityTestAction.BeforeTest(ITest test)
        {
            Debug.Assert(scene.EndsWith(".unity"));
            yield return EditorSceneManager.LoadSceneAsyncInPlayMode(scene, new LoadSceneParameters(LoadSceneMode.Single));
        }

        IEnumerator IOuterUnityTestAction.AfterTest(ITest test)
        {
            yield return null;
        }

        public IEnumerator BeforeTest(ITest test)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerator AfterTest(ITest test)
        {
            throw new System.NotImplementedException();
        }
    }




}
