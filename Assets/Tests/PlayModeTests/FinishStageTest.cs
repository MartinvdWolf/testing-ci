using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using NUnit.Framework.Interfaces;

public class FinishStageTest
{
    int left = -1;
    int right = 1;
    string goMessage;

    [UnitySetUp]
    public IEnumerator UnitySetUp()
    {
        yield return EditorSceneManager.LoadSceneAsyncInPlayMode("./Assets/Scenes/Stage.unity", new LoadSceneParameters(LoadSceneMode.Single));
    }

    [UnityTest]
    public IEnumerator RunStage()
    {
        EventManager.StartListening("GameOver", GameOver);
        yield return new WaitForSeconds(1);
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", left } });
        yield return new WaitForSeconds(2.5f);
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction",  right } });
        yield return new WaitForSeconds(1);
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", left } });
        yield return new WaitForSeconds(4);
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", right } });
        yield return new WaitForSeconds(0.5f);
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", right } });
        yield return new WaitForSeconds(5);
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", left } });
        yield return new WaitForSeconds(3.97f);
        EventManager.TriggerEvent("Jump", null);
        yield return new WaitForSeconds(1);
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", left } });
        yield return new WaitForSeconds(5);
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", right } });
        yield return new WaitForSeconds(0.5f);
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", right } });
        yield return new WaitForSeconds(6);
        EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", left } });
        yield return new WaitForSeconds(5.35f);
        EventManager.TriggerEvent("Jump", null);
        yield return new WaitForSeconds(10);
        Assert.AreEqual("win", goMessage);
        EventManager.StopListening("GameOver", GameOver);
        yield return null;
    }

    void GameOver(Dictionary<string, object> message)
    {
        goMessage = message["state"].ToString();
    }
}
