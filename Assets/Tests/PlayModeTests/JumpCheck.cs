using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class JumpCheck
{
    int sceneLoadDelay = 1;

    [UnitySetUp]
    public IEnumerator UnitySetUp()
    {
        Object.Destroy(GameObject.Find("EventManager"));
        yield return EditorSceneManager.LoadSceneAsyncInPlayMode("./Assets/Scenes/Stage.unity", new LoadSceneParameters(LoadSceneMode.Single));
        Debug.Log(SceneManager.GetActiveScene().name);
    }

    [UnityTest]
    public IEnumerator JumpStateTriggeredWithEnumeratorPasses()
    {
        //Scene loading grace period
        yield return new WaitForSeconds(sceneLoadDelay);

        //PlayerListener on Player uses enums for lanes, direction being a number is required to observe the correct outcome
        EventManager.TriggerEvent("Jump", null);

        var player = GameObject.Find("Player").GetComponent<PlayerListener>();

        Assert.AreEqual(true, player.isJumping);

    }
}
