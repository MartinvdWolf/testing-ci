using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectSpawner : MonoBehaviour
{
    enum Lanes { left, mid, right };

    [SerializeField] GameObject spawnObject;
    [SerializeField] float spawnPosZ;
    [SerializeField] float spawnPosY;
    [SerializeField] int spawnAmount;
    [SerializeField] float distanceBetweenSpawns;
    [SerializeField] Lanes spawnLane;
    private GameObject clone;
    Quaternion origRotation;
    // Start is called before the first frame update
    void Start()
    {
        origRotation = spawnObject.transform.rotation;
        float lanePos = spawnLane switch
        {
            Lanes.left => LevelManager.leftLanePosX,
            Lanes.right => LevelManager.rightLanePosX,
            _ => LevelManager.midLanePosX,
        };

        transform.position = new Vector3(lanePos, transform.position.y, transform.position.z);

        for (int i = 0; i < spawnAmount; i++)
        {
            clone = Instantiate(spawnObject, new Vector3(transform.position.x, spawnPosY, spawnPosZ + (spawnAmount * distanceBetweenSpawns)*i), origRotation, GameObject.FindGameObjectWithTag("mover").transform);
            clone.name = clone.name + i;
        }
    }


}
