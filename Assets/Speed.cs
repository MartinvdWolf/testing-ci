using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speed : MonoBehaviour
{
    [SerializeField] internal float baseSpeed = 10f;
    [SerializeField] internal float maxSpeed = 20f;
    [SerializeField] internal float curSpeed = 0.0f;
    [SerializeField] internal float accelRate = 0.01f;
    [SerializeField] internal float curAccel = 0.0f;
    [SerializeField] internal float maxAccel = 0.5f;
    [SerializeField] internal float slowDownSpd = 0.8f;
    [SerializeField] internal float slowDownAccel = 0.9f;
    [SerializeField] internal float slowDur = 0.3f;
    [SerializeField] internal bool isSlow = false;
    void Start()
    {

    }

    internal void OnEnable()
    {
        EventManager.StartListening("getHit", Slowdown);
    }

    internal void OnDisable()
    {
        EventManager.StopListening("getHit", Slowdown);
    }

    internal IEnumerator SlowTimer()
    {
        isSlow = true;
        yield return new WaitForSeconds(slowDur);
        isSlow = false;
    }

    void Slowdown(Dictionary<string, object> message)
    {
        StartCoroutine(SlowTimer());
    }

    // Update is called once per frame
    void Update()
    {
        curAccel += accelRate;
        curAccel = Mathf.Clamp(curAccel, 0, maxAccel);
        curSpeed += curAccel;
        curSpeed = Mathf.Clamp(curSpeed, 0, maxSpeed);
        if (isSlow)
        {
            curSpeed *= slowDownSpd;
            curAccel *= slowDownAccel;
        }
        transform.Translate(Vector3.back * curSpeed * Time.deltaTime);
    }

}
