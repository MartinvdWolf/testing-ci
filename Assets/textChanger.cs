using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class textChanger : MonoBehaviour
{
    
    public void UpdateText(string text)
    {
        gameObject.GetComponent<UnityEngine.UI.Text>().text = text;
    }
}
