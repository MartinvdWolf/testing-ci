using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerListener : MonoBehaviour
{
    internal enum Lanes { Left = 0, Mid = 1, Right = 2 };
    [SerializeField] internal Lanes myLane;

    Rigidbody rigidBody;
    Vector3 newPos = new Vector3();

    internal bool isHurting = false;
    internal bool isJumping = false;
    internal bool isMoving = false;
    internal float jumpForce = 4f;
    internal float defaultY = 0.2f;
    internal float defaultZ = -4.2f;
    internal float movementSpeed = 2f;
    internal int curLives = 3;
    internal bool isInvuln = false;
    internal float invulnDuration = 0.6f;

    internal void OnEnable()
    {
        EventManager.StartListening("Move", Move);
        EventManager.StartListening("Jump", Jump);
        EventManager.StartListening("getHit", Hurt);
    }
    internal void Awake()
    {
        this.GetComponent<Renderer>().material.color = Color.blue;
    }
    internal void Start()
    {

        transform.position = new Vector3(LevelManager.midLanePosX, defaultY, defaultZ);
        rigidBody = GetComponent<Rigidbody>();
        Lanes myLane = Lanes.Mid;
    }
    internal void OnDisable()
    {
        EventManager.StopListening("Move", Move);
        EventManager.StopListening("Jump", Jump);
        EventManager.StopListening("getHit", Hurt);
    }

    internal void Move(Dictionary<string, object> message)
    {
        Debug.Log(message["direction"]);
        var direction = (int)message["direction"];
        
        if (!isMoving)
        {
            isMoving = true;
            if (myLane == Lanes.Mid)
            {
                myLane += direction;
            }
            else if(myLane == Lanes.Right && direction != 1)
            {
                myLane += direction;
            }
            else if(myLane == Lanes.Left && direction != -1)
            {
                myLane += direction;
            }

            //Ooooh nieuwe C# switch expression 
            newPos.x = myLane switch
            {
                Lanes.Left => LevelManager.leftLanePosX,
                Lanes.Right => LevelManager.rightLanePosX,
                _ => LevelManager.midLanePosX,
            };
        }
    }

    internal void Jump(Dictionary<string, object> message)
    {
        Debug.Log("Jump!");
        isJumping = true;
    }
    
    internal void Hurt(Dictionary<string, object> message)
    {
        if (!isInvuln)
        {
            curLives--;
            if (curLives == 0)
            {
                EventManager.TriggerEvent("GameOver", new Dictionary<string, object> { { "lose", null} });
                return;
            }
            StartCoroutine(Invulnerable());
        }
    }

    internal IEnumerator Invulnerable()
    {
        isInvuln = true;
        this.GetComponent<Renderer>().material.color = Color.white;
        yield return new WaitForSeconds(invulnDuration);
        this.GetComponent<Renderer>().material.color = Color.blue;
        isInvuln = false;
    }


    internal void Update()
    {
        if (isMoving)
        {
            transform.position = new Vector3(Mathf.MoveTowards(transform.position.x, newPos.x, movementSpeed * Time.deltaTime), transform.position.y, transform.position.z);
            if (Mathf.Abs(transform.position.x - newPos.x) < 0.01f)
            {
                transform.position = new Vector3(newPos.x, transform.position.y, transform.position.z);
                isMoving = false;
            }
        }
    }
    internal void FixedUpdate()
    {
        if (isJumping)
        {
            rigidBody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isJumping = false;
        }
    }
}
