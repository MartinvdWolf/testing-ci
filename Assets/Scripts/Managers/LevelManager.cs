using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static float leftLanePosX = -0.75f;
    public static float midLanePosX = 0;
    public static float rightLanePosX = 0.75f;
    public Vector3 levelStartPos;
    public int levelProgress;
    GameObject objMover;

    private void Start()
    {
        objMover = GameObject.FindGameObjectWithTag("mover");
        levelStartPos = objMover.transform.position;
        StartCoroutine(LevelStartDelay());
    }

    private void Awake()
    {
        LevelStartDelay();   
    }
    //Finish game state is also triggered with GameOver, but a different message.
    private void OnEnable()
    {
        EventManager.StartListening("GameOver", GameOver);
    }

    private void OnDisable()
    {
        EventManager.StopListening("GameOver", GameOver);
    }

    void GameOver(Dictionary<string, object> message)
    {
        StartCoroutine(GODelay());
    }

    IEnumerator GODelay()
    {
        yield return new WaitForSecondsRealtime(2);
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(5);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    IEnumerator LevelStartDelay()
    {
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(3);
        Time.timeScale = 1;
    }

    private void Update()
    {
        if (objMover)
        {
            Vector3 distance = objMover.transform.position - levelStartPos;
            levelProgress = Mathf.RoundToInt(Mathf.Abs(distance.z));
        }
    }
}

