﻿using System.Runtime.CompilerServices;

[assembly:InternalsVisibleTo("EditModeTests")]
[assembly: InternalsVisibleTo("PlayModeTests")]