using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{

    internal static float coinValue = 1;
    private void OnTriggerEnter(Collider other)
    {
        if (CompareTag("deleter"))
        {
            Destroy(other.gameObject);
            return;
        }

        if (CompareTag("finish"))
            EventManager.TriggerEvent("GameOver", new Dictionary<string, object> { { "state", "win" } });

        if (other.name.Contains("Coin"))
        {
            EventManager.TriggerEvent("getCoin", new Dictionary<string, object> { { "amount", coinValue } });
            //Debug.Log("Get " + other.name);
            Destroy(other.gameObject);
        }

        if(other.name.Contains("Blocker"))
        {
            EventManager.TriggerEvent("getHit", new Dictionary<string, object> { { "slowdown", null} });
            //Debug.Log("Hit " + other.name);
            Destroy(other.gameObject);
        }
    }
}
