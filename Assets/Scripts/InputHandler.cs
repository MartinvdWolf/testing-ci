using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    internal int left = -1;
    internal int right = 1;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
            EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", left } });

        if (Input.GetKeyDown(KeyCode.D))
            EventManager.TriggerEvent("Move", new Dictionary<string, object> { { "direction", right } });

        if (Input.GetKeyDown(KeyCode.Space))
            EventManager.TriggerEvent("Jump", null);
    }


}
